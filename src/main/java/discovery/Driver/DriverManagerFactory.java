package discovery.Driver;

public class DriverManagerFactory {
    public static DriverManager getManager(String type) {

        DriverManager driverManager = null;
        DriverType driverType = DriverType.valueOf(type.toUpperCase());

        switch (driverType.toString()) {
            case "CHROME":
                driverManager = new ChromeDriverManager();
                break;
            case "FIREFOX":
                driverManager = new FirefoxDriverManager();
                break;
            case "SAFARI":
                driverManager = new SafariDriverManager();
                break;
            default:
                System.out.println("Please provide a valid DriverType");
                break;
        }
        return driverManager;
    }
}
