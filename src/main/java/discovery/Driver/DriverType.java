package discovery.Driver;

public enum DriverType {
    CHROME,
    FIREFOX,
    SAFARI;
}
