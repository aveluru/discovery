package discovery.Utils;

import cucumber.api.Scenario;
import discovery.Runner.RunCukesTest;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;

public class Hooks_Methods extends RunCukesTest {

    public static void captureScreenShot(Scenario scenario) throws IOException {
        String base64Screenshot = "data:image/png;base64," + ((TakesScreenshot) RunCukesTest.driver).getScreenshotAs(OutputType.BASE64);
        String str_StepName = scenario.getName() + " has failed. Please see the screenshot below.";

        File srcFile = ((TakesScreenshot) RunCukesTest.driver).getScreenshotAs(OutputType.FILE);

        byte[] data = FileUtils.readFileToByteArray(srcFile);
        scenario.embed(data, "image/png");
        Log.info("The page source is: " + RunCukesTest.driver.getPageSource());
    }
}
