package discovery.Utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.jayway.jsonpath.JsonPath;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class JsonParsing {

    public static HashMap<String, String> jsontoMap(String jsonFileName) {
        try {
            Path path = Paths.get("./src/main/resources/" + jsonFileName + ".json");
            //Reading the OR JSON file
            String json = new String(Files.readAllBytes(path));

            //Needed to have a map of the JSON values
            ObjectMapper mapper = new ObjectMapper();
            //Final map with all JSON values as key pair
            HashMap<String, String> map = new HashMap<String, String>();
            //Parser to parse the JSON file
            JSONParser parser = new JSONParser();

            Object obj = parser.parse(json);
            org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;

            //Set to store all the page names from the OR JSON file
            Set<String> mySet = jsonObject.keySet();
            for (String key : mySet) {
                map.putAll(mapper.readValue(JsonPath.read(json, "$." + key).toString(), new TypeReference<Map<String, String>>() {
                }));
                //map = mapper.readValue(JsonPath.read(json, "$."+key).toString(), new TypeReference<Map<String, String>>(){});
            }
            Log.info("The OR map is:\n" + map);
            return map;
        } catch (IOException e) {
            Log.info("We have encountered an IOEXception and details are : " + e);
            e.printStackTrace();
        } catch (ParseException e) {
            Log.info("We have encountered an ParseException and details are : " + e);
            e.printStackTrace();
        } catch (Exception e) {
            Log.info("We have encountered an Exception and details are : " + e);
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isJSONValid(String jsonInString) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.readTree(jsonInString);
            return true;
        } catch (JsonProcessingException e) {
            Log.info("we have encounter an JsonProcessingException while reading json String and details are : " + e);
            e.printStackTrace();
        } catch (IOException e) {
            Log.info("we have encounter an IOException while reading json String and details are : " + e);
            e.printStackTrace();
        } catch (Exception e) {
            Log.info("we have encounter an Exception while reading json String and details are : " + e);
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isValid(String json) {
        try {
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            Log.info("We have encountered JsonSyntaxException and details are : " + jse);
            jse.printStackTrace();
        }
        return false;
    }

    public static HashMap<String, String> generateMapDesiredCapabilities(String jsonFileName) {
        try {
            Path path = Paths.get("./src/main/resources/" + jsonFileName + ".json");
            //Reading the OR JSON file
            String json = new String(Files.readAllBytes(path));

            //Needed to have a map of the JSON values
            ObjectMapper mapper = new ObjectMapper();
            //Final map with all JSON values as key pair
            HashMap<String, String> map = new HashMap<String, String>();
            //Parser to parse the JSON file
            JSONParser parser = new JSONParser();

            Object obj = parser.parse(json);
            org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;

            //String currentExecutionDevice = jsonObject.get("ExecutionDevice").toString();

            map.putAll(mapper.readValue(JsonPath.read(json, "$.." ).toString(), new TypeReference<Map<String, String>>() {
            }));

            Log.info("The desired capabilities map is:\n" + map);
            return map;
        } catch (IOException e) {
            Log.info("We have encounter an Exception while reading bytes from file and details are : " + e);
            e.printStackTrace();
        } catch (ParseException e) {
            Log.info("We have encounter an Exception while parsing the json and details are : " + e);
            e.printStackTrace();
        } catch (Exception e) {
            Log.info("We have encounter an Exception and details are : " + e);
            e.printStackTrace();
        }
        return null;
    }
}
