package discovery.Utils;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

public class LoggerFilter extends Filter<ILoggingEvent>
{
    @Override
    public FilterReply decide(ILoggingEvent event) {
        //Making sure that the Appium specific logs are not getting logged into the test scripts
        //The same can be done just by adding event.getLoggerName().startsWith("org.apache.http") alone
        //Here all of the appium specific stuffs are mentioned for brevity
        if(event.getLoggerName().startsWith("org.apache.http.impl.conn.DefaultHttpClientConnectionOperator")
                || event.getLoggerName().startsWith("org.apache.http.headers")
                || event.getLoggerName().startsWith("org.apache.http.impl.execchain.MainClientExec")
                || event.getLoggerName().startsWith("org.apache.http.impl.conn.PoolingHttpClientConnectionManager")
                || event.getLoggerName().startsWith("org.apache.http.impl.conn.DefaultManagedHttpClientConnection")
                || event.getLoggerName().startsWith("org.apache.http.client.protocol.RequestAuthCache")
                || event.getLoggerName().startsWith("org.apache.http.client.protocol.RequestAddCookies")
                || event.getLoggerName().startsWith("org.apache.http.wire")
                || event.getLoggerName().startsWith("org.apache.http")
                || event.getLoggerName().startsWith("com.epam"))
        {
            return FilterReply.DENY;
        }
        else
        {
            return FilterReply.ACCEPT;
        }
    }
}
