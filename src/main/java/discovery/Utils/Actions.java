package discovery.Utils;


import discovery.Runner.RunCukesTest;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Actions extends RunCukesTest {

    //https://stackoverflow.com/questions/11736027/webdriver-wait-for-element-using-java
    public static boolean waitForVisibilityofElement(WebElement element) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("about_me")));
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("We could not find " + element + " in the given time, please refer the error details" + e.getStackTrace());
            return false;
        }
    }

    public static boolean checkIfElementIsVisibleIfNotSwipeInSpecifiedDirection(WebElement element, DiscoveryEnums.ScrollDirections scrollDirection) {
        try {
            Log.info("Need to scroll to the element");

            //Log.info("Element is not displayed on the current screen. So scrolling " + scrollDirection);

            /*JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView();", element);*/


            Point hoverItem = element.getLocation();
            //((JavascriptExecutor) driver).executeScript("return window.title;");
            Thread.sleep(6000);
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0," + (hoverItem.getY()) + ");");
            // Adjust your page view by making changes right over here (hoverItem.getY()-400)

            Log.info("We have scrolled to");
            //checkIfElementIsVisibleIfNotSwipeInSpecifiedDirection(element, scrollDirection);


            return true;
        } catch (Exception e) {
            System.out.println("We were unable to scroll to " + element + " , please refer to below error details.");
            System.out.println("Error Detail are: \n" + e.getMessage());
            return false;
            //Log.error("Error in Actions : checkIfElementIsVisibleIfNotSwipeInSpecifiedDirection()");
            //Log.error("The error message is:\n" + e.getMessage());
        }
    }

    public static boolean click(WebElement element) {
        try {
            element.click();
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("We could not find " + element + " in the given time, please refer the error details" + e.getStackTrace());
            return false;
        }
    }

    public static void waitForPageLoading() {
        long pageLoadTimeout = Long.parseLong(mapConfigProperties.get("PageLoadTimeout"));
        new WebDriverWait(driver, pageLoadTimeout).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }

    public static void waitForElement(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 120);
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='text3']")));
        wait.until(ExpectedConditions.visibilityOf(element));
    }
}
