package discovery.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

public class EventCapture implements WebDriverEventListener {

    @Override
    public void beforeAlertAccept(WebDriver driver) {

    }

    @Override
    public void afterAlertAccept(WebDriver driver) {

    }

    @Override
    public void afterAlertDismiss(WebDriver driver) {

    }

    @Override
    public void beforeAlertDismiss(WebDriver driver) {

    }

    @Override
    public void beforeNavigateTo(String url, WebDriver driver) {
        System.out.println("We are going to Navigate to url: " + url);
        Log.info("We are going to Navigate to url: " + url);
    }

    @Override
    public void afterNavigateTo(String url, WebDriver driver) {
        System.out.println("We have Navigated to url: " + url);
        Log.info("We have Navigated to url: " + url);
    }

    @Override
    public void beforeNavigateBack(WebDriver driver) {

    }

    @Override
    public void afterNavigateBack(WebDriver driver) {

    }

    @Override
    public void beforeNavigateForward(WebDriver driver) {
        System.out.println("We are going perform a Forward Navigation");
        Log.info("We are going perform a Forward Navigation");
    }

    @Override
    public void afterNavigateForward(WebDriver driver) {
        System.out.println("We have performed a Forward Navigation");
        Log.info("We have performed a Forward Navigation");
    }

    @Override
    public void beforeNavigateRefresh(WebDriver driver) {

    }

    @Override
    public void afterNavigateRefresh(WebDriver driver) {

    }

    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        System.out.println("We are going to find element " + element + " using By criteria " + by.toString());
        Log.info("We are going to find element " + element + " using By criteria " + by.toString());
    }

    @Override
    public void afterFindBy(By by, WebElement element, WebDriver driver) {
        System.out.println("We have found element " + element + " using By criteria " + by.toString());
        Log.info("We have found element " + element + " using By criteria " + by.toString());
    }

    @Override
    public void beforeClickOn(WebElement element, WebDriver driver) {
        System.out.println("We are going to click on Element " + element);
        Log.info("We are going to click on Element " + element);
    }

    @Override
    public void afterClickOn(WebElement element, WebDriver driver) {
        System.out.println("We have clicked on Element " + element);
        Log.info("We have clicked on Element " + element);
    }

    @Override
    public void beforeChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {

    }

    @Override
    public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {

    }

    @Override
    public void beforeScript(String script, WebDriver driver) {

    }

    @Override
    public void afterScript(String script, WebDriver driver) {

    }

    @Override
    public void beforeSwitchToWindow(String windowName, WebDriver driver) {

    }

    @Override
    public void afterSwitchToWindow(String windowName, WebDriver driver) {

    }

    @Override
    public void onException(Throwable throwable, WebDriver driver) {

    }

    @Override
    public <X> void beforeGetScreenshotAs(OutputType<X> target) {

    }

    @Override
    public <X> void afterGetScreenshotAs(OutputType<X> target, X screenshot) {

    }

    @Override
    public void beforeGetText(WebElement element, WebDriver driver) {
        Log.info("We are going to extract the text of Element " + element);
    }

    @Override
    public void afterGetText(WebElement element, WebDriver driver, String text) {
        Log.info("We have extracted text of Element " + element);
    }
}
