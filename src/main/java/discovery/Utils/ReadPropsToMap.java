package discovery.Utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ReadPropsToMap {
    public static Map<String, String> readPropsToMap(String strPropFilePath, String strDelimiter) {
        Map<String, String> retMapOfProps = new HashMap<String, String>();
        try {
            @SuppressWarnings("resource")
            BufferedReader br = new BufferedReader(new FileReader(strPropFilePath));
            String line;
            while ((line = br.readLine()) != null) {
                if (!(line.trim().length() == 0)) {
                    if (!(line.startsWith("#"))) {
                        int intPos = line.indexOf(strDelimiter);
                        retMapOfProps.put(line.substring(0, intPos).trim(), line.substring(intPos + 1).trim());
                    }
                }
            }
            return retMapOfProps;
        } catch (FileNotFoundException e) {
            Log.info("We have encounter an Exception while reading the file and details are : " + e);
            e.printStackTrace();
        } catch (IOException e) {
            Log.info("We have encounter an Exception while reading each line and details are : " + e);
            e.printStackTrace();
        } catch (Exception e) {
            Log.info("We have encounter an Exception and details are : " + e);
            e.printStackTrace();
        }
        return null;
    }
}
