package discovery.Utils;

import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Log {
    //Initializing a log object which is static of the type log4j
    private static Logger log = LoggerFactory.getLogger(Log.class);

    public static synchronized void beginTestCase(String strTestCaseName) {
        log.info("***************************************************************************************************************************");
        log.info("---------------------------------------Beginning of "+strTestCaseName+"----------------------------------------------------");
        log.info("***************************************************************************************************************************");
    }

    public static synchronized void endTestCase(String strTestCaseName) {
        log.info("***************************************************************************************************************************");
        log.info("---------------------------------------Ending of "+strTestCaseName+"----------------------------------------------------");
        log.info("***************************************************************************************************************************");
    }

    public static synchronized void info(String message) {
        log.info(message);
    }

    public static synchronized void warn(String message) {
        log.warn(message);
    }

    public static synchronized void error(String message) {
        log.error(message);
    }

    public static synchronized  void trace(String message) {
        log.trace(message);
    }

    public static synchronized void debug(String message) {
        log.debug(message);
    }

    public static String prettyprintJsonResponse(String strResponse) {
        String response = null;
        try
        {
            com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
            Object json = mapper.readValue(strResponse, Object.class);
            //System.out.println("This is deprecated"+mapper.defaultPrettyPrintingWriter().writeValueAsString(json));
            //response = mapper.enable(com.fasterxml.jackson.databind.SerializationConfig. .INDENT_OUTPUT).writeValueAsString(json);
            response = mapper.enable(SerializationFeature.INDENT_OUTPUT).writeValueAsString(json);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return response;
    }
}
