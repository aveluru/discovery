package discovery.StepDefinition;

import cucumber.api.Scenario;
import discovery.Runner.RunCukesTest;
import discovery.Utils.Hooks_Methods;
import discovery.Utils.Log;
import discovery.Utils.TestLogHelper;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends RunCukesTest {

    @Before("@AddFavorite")
    public void beforeFirst(Scenario scenario) {
        TestLogHelper.startTestCaseLogging(scenario.getName());
        Log.info("We are executing Before hooks @AddFavoriteSteps for " + scenario.getName());
    }

    @After("@AddFavorite")
    public void afterFirst(Scenario scenario) {
        Log.info("We are executing After hooks @AddFavoriteSteps for " + scenario.getName());
        try {
            if (scenario.isFailed())
                Hooks_Methods.captureScreenShot(scenario);
            TestLogHelper.stopTestLogging();
        } catch (Exception e) {
            Log.info("Unable to capture screenshot" + e.getMessage());
            Log.info(e.getStackTrace().toString());
        }
    }

}
