package discovery.StepDefinition;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import discovery.Pages.MyVideos;
import discovery.Runner.RunCukesTest;

import static discovery.StepDefinition.AddFavoriteSteps.myVideos;

public class MyVideosSteps extends RunCukesTest {

    //MyVideos myVideos = new MyVideos(driver);
    MyVideos myVideos1;

    @Then("I validate favorite show title")
    public void validateTitles() {
        myVideos1 = myVideos.validateFavoriteShowTitle();
    }

    @And("I validate favorite show description")
    public void extractValue() {
        myVideos1 = myVideos.validateFavoriteShowDescription();
    }
}
