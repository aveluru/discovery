package discovery.StepDefinition;

import cucumber.api.java.en.And;
import discovery.Pages.MyVideos;
import discovery.Runner.RunCukesTest;
import com.google.common.base.Objects;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import discovery.Pages.HomePage;
import discovery.Utils.Actions;
import org.junit.Assert;

public class AddFavoriteSteps extends RunCukesTest {

    HomePage homePage = new HomePage(driver);
    public static MyVideos myVideos;

    @Given("^I launch Discovery$")
    public void launchDiscovery() throws Throwable {
        //https://stackoverflow.com/questions/46225066/how-to-know-webdriver-opened-url-successfully
        String str_BaseURL = "https://www.discovery.com/";
        driver.get(str_BaseURL);
        Actions.waitForPageLoading();
        Assert.assertTrue(Objects.equal(driver.getCurrentUrl(), str_BaseURL));

    }

    @Then("^I add (\\d+) videos to my favorite$")
    public void addFavorite(int numberOfVideos) throws Throwable {
        homePage = homePage.addFavorites(numberOfVideos);
    }

    @And("^I view menu items$")
    public void viewMenuItems() throws Throwable {
        homePage = homePage.viewMenu();
    }

    @And("^I view my videos$")
    public void viewMyVideos() throws Throwable {
        myVideos = homePage.viewMyVideos();
    }
}
