package discovery.Pages;

import discovery.Utils.Actions;
import discovery.Utils.Log;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class HomePage {

    private WebDriver driver;

    @FindAll({@FindBy(xpath = "//*[@id='react-root']//h2//*[text()='Recommended']/parent::h2/following-sibling::div/div/div[1]/div[1]/div/section/div[3]/a/div/div[2]/div/span/i")})
    private List<WebElement> favoriteLink;

    @FindAll({@FindBy(xpath = "//*[@id='react-root']//h2//*[text()='Recommended']/parent::h2/following-sibling::div/div/div[1]/div[1]/div/section//*/h3/div")})
    private List<WebElement>  favoriteLinkTitle;

    @FindAll({@FindBy(xpath = "//*[@id='react-root']//h2//*[text()='Recommended']/parent::h2/following-sibling::div/div/div[1]/div[1]/div/section//*/div[1]/div[1]")})
    private List<WebElement>  favoriteLinkDescription;

    @FindBy(className = "icon-menu")
    private WebElement menu;

    @FindBy(xpath = "//*[@id='react-root']//*/header/nav//*/a[@href='/my-videos']")
    private WebElement myVideos;

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public HomePage addFavorites(int videoNumber) {
        for(int i=0; i<videoNumber; i++) {
            //Extracting First Movie Title and Description under recommended for you
            System.setProperty("FavoriteLinkTitle" + String.valueOf(i), getFavoriteLinkTitle(favoriteLinkTitle.get(i)));
            System.setProperty("FavoriteLinkDescription" + String.valueOf(i), getFavoriteLinkDescription(favoriteLinkDescription.get(i+1)));
            //Add First Movie to Favorites under recommended for you
            Assert.assertTrue(Actions.click(favoriteLink.get(i)));
        }
        return this;
    }

    public HomePage viewMenu() {
        Assert.assertTrue(Actions.click(menu));
        Actions.waitForPageLoading();
        return this;
    }

    public MyVideos viewMyVideos() {
        Assert.assertTrue(Actions.click(myVideos));
        Actions.waitForPageLoading();
        return new MyVideos(driver);
    }

    public String getFavoriteLinkTitle(WebElement element) {
        String strReturnValue = element.getAttribute("innerText");
        Log.info("Movie Title is: " + strReturnValue);
        return strReturnValue;
    }

    public String getFavoriteLinkDescription(WebElement element) {
        String strReturnValue = element.getAttribute("innerText");
        Log.info("Movie Description is: " + strReturnValue);
        return strReturnValue;
    }
}
