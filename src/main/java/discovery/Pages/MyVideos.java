package discovery.Pages;

import discovery.Utils.Log;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MyVideos {
    private WebDriver driver;

    @FindAll({@FindBy(xpath = "//*[@id='react-root']//*/h2[text()='Favorite Shows']/following-sibling::div[1]//*/div/section")})
    private List<WebElement> favorites;

    @FindBy(xpath = "//*[@id='react-root']//*/h2[text()='Favorite Shows']/following-sibling::div[1]//*/div/section/div[3]/a/div/h3/div")
    private List<WebElement> favoriteLinkTitle;

    @FindBy(xpath = "//*[@id='react-root']//*/h2[text()='Favorite Shows']/following-sibling::div[1]//*/div/section/div[3]/a/div/div[1]/div")
    private List<WebElement> favoriteLinkDescription;

    public MyVideos(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public MyVideos validateFavoriteShowTitle() {
        //Actions.waitForElement(firstFavoriteLinkTitle1);

        int numbeOfFavoriteVideos = favorites.size();
        ArrayList<String> ExpectedMovieTitle = new ArrayList<>();

        for (int i = 0; i < numbeOfFavoriteVideos; i++)
            ExpectedMovieTitle.add(getFavoriteLinkTitle(favoriteLinkTitle.get(i)));

        for (int j = 0; j < numbeOfFavoriteVideos; j++)
            Assert.assertTrue(ExpectedMovieTitle.contains(System.getProperty("FavoriteLinkTitle" + String.valueOf(j))));
        return this;
    }

    public MyVideos validateFavoriteShowDescription() {
        //Actions.waitForElement(firstFavoriteLinkTitle1);
        boolean flag = true;

        int numbeOfFavoriteVideos = favorites.size();
        ArrayList<String> ExpectedMovieDescription = new ArrayList<>();

        for (int i = 0; i < numbeOfFavoriteVideos; i++)
            ExpectedMovieDescription.add(getFavoriteLinkDescription(favoriteLinkDescription.get(i)));

        /*for(int j=0; j<numbeOfFavoriteVideos; j++)
            Assert.assertTrue(ExpectedMovieDescription.contains(System.getProperty("FavoriteLinkDescription"+String.valueOf(j))));*/
        for (int j = 0; j < numbeOfFavoriteVideos; j++)
            for (String obj : ExpectedMovieDescription) {
                if (obj.length() == System.getProperty("FavoriteLinkDescription" + String.valueOf(j)).length()) {
                    if (Objects.equals(System.getProperty("FavoriteLinkDescription" + String.valueOf(j)), obj)){
                        flag = true;
                        break;
                    }
                    else
                        flag = false;
                } else if (obj.length() > System.getProperty("FavoriteLinkDescription" + String.valueOf(j)).length()) {
                    if (System.getProperty("FavoriteLinkDescription" + String.valueOf(j)).contains(obj.substring(0, System.getProperty("FavoriteLinkDescription" + String.valueOf(j)).length() - 1))) {
                        flag = true;
                        break;
                    }
                    else
                        flag = false;
                } else
                    flag = false;
            }
        if (flag)
            Assert.assertTrue(true);
        else
            Assert.assertTrue(false);
        return this;
    }

    public String getFavoriteLinkTitle(WebElement element) {
        String strReturnValue = element.getAttribute("innerText");
        Log.info("Movie Title is: " + strReturnValue);
        return strReturnValue;
    }

    public String getFavoriteLinkDescription(WebElement element) {
        String strReturnValue = element.getAttribute("innerText");
        Log.info("Movie Description is: " + strReturnValue);
        return strReturnValue;
    }

}
