package discovery.Runner;


import ch.qos.logback.classic.util.ContextInitializer;
import com.github.mkolisnyk.cucumber.runner.AfterSuite;
import com.github.mkolisnyk.cucumber.runner.BeforeSuite;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import discovery.Driver.DriverManager;
import discovery.Driver.DriverManagerFactory;
import discovery.Utils.*;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Map;
import java.util.concurrent.TimeUnit;

//import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(
        reportPrefix = "${ReportName}",
        jsonReport = "target/cucumber.json",
        retryCount = 0,
        screenShotSize = "100px",
        screenShotLocation = "Screenshots/",
        detailedReport = true,
        detailedAggregatedReport = true,
        systemInfoReport = true,
        overviewReport = true,
        coverageReport = true,
        jsonUsageReport = "target/cucumber-usage.json",
        usageReport = true,
        toPDF = true,
        overviewChartsReport = true,
        pdfPageSize = "A4 Landscape",
        outputFolder = "./${ReportsFolder}/"
)
@CucumberOptions(
        strict = true,
        monochrome = true,
        dryRun = false,
        features = "src/main/resources/Features/",
        glue = "discovery.StepDefinition",
        tags = "@Smoke",
        plugin = {"html:target/cucumber-html-report",
                "json:target/cucumber.json", "pretty:target/cucumber-pretty.txt",
                "usage:target/cucumber-usage.json", "junit:target/cucumber-results.xml",
        }
)
public class RunCukesTest {

    protected static DriverManager driverManager;
    protected static WebDriver driver = null;
    protected static String strFolderName = null;
    protected static Logger log = LoggerFactory.getLogger(RunCukesTest.class);
    public static Map<String, String> mapConfigProperties;

    @BeforeSuite
    public static void createDetailedTestLog() {
        System.setProperty(ContextInitializer.CONFIG_FILE_PROPERTY, "./logback.xml");
        strFolderName = CreateFolder.getDateTime();
        System.setProperty("ReportsFolder", CreateFolder.createFolder("Reports", strFolderName));
        System.setProperty("logsfolder", CreateFolder.createFolder("Logs", strFolderName));

        log = LoggerFactory.getLogger(RunCukesTest.class);
        TestLogHelper.startTestCaseLogging("RunCukesTest");
        log.info("Execution of Selenium setUp began");

        mapConfigProperties = ReadPropsToMap.readPropsToMap("./src/main/resources/Configurartion.properties","=");
        System.out.println("Configsmap:\n" + mapConfigProperties);
        Log.info("Configsmap:\n" + mapConfigProperties);

        driverManager = DriverManagerFactory.getManager(mapConfigProperties.get("BrowserToExecute"));
        System.setProperty("ReportName", mapConfigProperties.get("ReportName")+"_"+mapConfigProperties.get("BrowserToExecute").toUpperCase());
        driver = driverManager.getWebDriver();
        driver.manage().timeouts().implicitlyWait(Integer.parseInt(mapConfigProperties.get("ImplicitWaitTime")), TimeUnit.SECONDS);

        TestLogHelper.stopTestLogging();
    }

    @AfterSuite
    public static void tearDown() {
        driverManager.quitDriver();
        Log.endTestCase("hooks");
        TestLogHelper.stopTestLogging();
    }
}
