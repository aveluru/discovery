@AddFavorite
Feature: Add to Favorites Feature
  We are going to handle all the Add to Favorite Scenarios as part of this feature.

  Background:
    Given I launch Discovery

  @Smoke
  Scenario: Smoke
    #Then I add videos to my favorite
    Then I add 2 videos to my favorite
    And I view menu items
    And I view my videos
    Then I validate favorite show title
    And I validate favorite show description
