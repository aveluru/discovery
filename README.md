**Executions Instructions:**
The project is going to use “gecko driver” to run all the tests on the browser. Since gecko driver is an executable file we need to download or install it manually on the execution machine. Better to install it than to download and set the path.

Installation Instructions:
1.	We need to install gecko driver or update it using “Homebrew” (open-source software package management system that simplifies the installation of software on Apple's macOS operating system and Linux.). Use the below command:

    *ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"*

2.	Now we can install the gecko driver on the system by using the below command:

    *brew install gecko driver*

We need to run the above commands on terminal from user’s home directory.

We can run the project in two ways either from the IDE or by using command file from terminal.

**From IDE:**
Either clone the repository from GitLab or download as zip and import it to IDE.
After which we can use “RuncukesTest” class from “Runner” package.

**From Terminal:**
We need to create a file with an extension “.command” and paste the below commands into the file.

#Go to path of the Project

    cd /Users/aveluru/Documents/discovery

#Perform a maven clean and build the executable jar file

    mvn clean install

#Run the executable jar file

    java -jar /Users/aveluru/Documents/discovery/target/discovery-1.0-SNAPSHOT-jar-with-dependencies.jar

After which open terminal and run the below command to give executable permission to the above created file.

    chmod -x <path to the command file including the file>
